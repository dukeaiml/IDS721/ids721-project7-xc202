## Data Processing with Vector Database

The Qdrant Data Management project aims to facilitate efficient data management and retrieval using the Qdrant vector search engine. It provides functionalities for inserting, querying, visualizing, and aggregating data stored in Qdrant collections.

In the project, we use Docker command to run a Qdrant container with specified port mappings and configuration for the gRPC port. It allows the Qdrant service to be accessed via port 6334 on the host machine, while also exposing port 6333 for potential other purposes. Make sure to install `Docker` before starting the project. 

```shell
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

The project can be divided into several parts, which includes Qdrant connection, data generation, data insertion, data querying, data visualization and data aggregation. 

### Qdrant Client:
Provides an interface to interact with the Qdrant vector search engine. It includes methods for creating collections, inserting data, querying data, and retrieving collection information.

![Screenshot 2024-03-20 at 5.42.31 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.42.31%20PM.png)

### Data Generation
Utilizes random data generation techniques to create sample data for insertion into Qdrant collections. This includes generating random strings for names, random age values, and computing hashcode for additional information.

![Screenshot 2024-03-20 at 5.58.22 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.58.22%20PM.png)

### Data Insertion
Inserts generated data into Qdrant collections by constructing payloads and points, then upsetting them into the specified collection. Each data point consists of the following information: 
   * ID: Unique identifier for each data point, formatted as "ID Number: <number>".
   * Name: Randomly generated username consisting of alphanumeric characters.
   * Age: Randomly generated age between 10 and 50 years.
   * Others: Additional information including a hashcode computed based on the ID.

![Screenshot 2024-03-20 at 5.44.07 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.44.07%20PM.png)

### Data Querying
Queries data from Qdrant based on specified search conditions such as age range (20 to 30 for example). It constructs search requests, sends them to Qdrant, and retrieves search results for further processing.

![Screenshot 2024-03-20 at 5.45.39 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.45.39%20PM.png)

The search results can be viewed in output_search.txt:

![Screenshot 2024-03-20 at 5.47.05 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.47.05%20PM.png)

### Data Aggregation
Aggregates data from search results to derive meaningful insights. This includes computing statistics such as the average age of retrieved data points.

![Screenshot 2024-03-20 at 5.48.30 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.48.30%20PM.png)

![Screenshot 2024-03-20 at 5.25.52 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.25.52%20PM.png)

### Data Visualization
Visualizes search results obtained from Qdrant by printing out relevant information for each retrieved data point. This includes ID, Name, Age, and any additional information.

![Screenshot 2024-03-20 at 5.47.58 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.47.58%20PM.png)

![Screenshot 2024-03-20 at 5.25.41 PM.png](screenshots%2FScreenshot%202024-03-20%20at%205.25.41%20PM.png)


