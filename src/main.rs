use anyhow::Result;
use qdrant_client::prelude::{QdrantClient, Distance, Payload};
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{Condition, CreateCollection, Filter, PointStruct, Range, SearchPoints, SearchResponse, VectorParams, VectorsConfig};
use serde_json::json;
use rand::Rng;
use crypto_hash::{Algorithm, hex_digest};
use std::fs::File;
use std::io::Write;

fn generate_random_string(length: usize) -> String {
    let rng = rand::thread_rng();
    let random_string: String = rng.sample_iter(rand::distributions::Alphanumeric)
        .take(length)
        .map(char::from)
        .collect();
    random_string
}

#[tokio::main]
async fn main() -> Result<()> {
    // connect to Qdrant server
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    let collection_name = "qdrant_collection";
    client.delete_collection(collection_name).await?;

    // create collection in Qdrant
    let create_collection_request = CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 50,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    };

    client.create_collection(&create_collection_request).await?;

    // get collection info from Qdrant
    let collection_info = client.collection_info(collection_name).await?;
    dbg!(collection_info);

    // insert data into Qdrant
    insert(&client, collection_name).await?;

    // query data from Qdrant
    let search_points_response = query(&client, collection_name).await?;

    // aggregation data from Qdrant
    aggregation(search_points_response).await?;

    Ok(())
}

async fn insert(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // insert data into Qdrant
    let mut payloads = Vec::new();
    for i in 0..10 {
        let payload: Payload = json!({
            "ID": format!("ID Number: {}", i),
            "Name": format!("Username: {}", generate_random_string(10)),
            "Age": rand::thread_rng().gen_range(10..=50),
            "Others": {
                "hashcode": format!("{}", hex_digest(Algorithm::SHA256, i.to_string().as_bytes()))
            }
        }).try_into().unwrap();
        payloads.push(payload);
    }

    let points = payloads.iter().enumerate().map(|(i, payload)| {
        PointStruct::new(i as u64, vec![0.; 50], payload.clone())
    }).collect::<Vec<_>>();

    client.upsert_points_blocking(collection_name, None, points, None).await?;
    println!("Data inserted successfully!");
    Ok(())
}

async fn query(client: &QdrantClient, name: &str) -> Result<SearchResponse> {

    let conditions = vec![
        Condition::range("Age", Range {
            lt: None,
            gt: None,
            gte: Some(20.0),
            lte: Some(30.0),
        }),
    ];

    let filter = Filter::all(conditions);

    let search_points_request = SearchPoints {
        collection_name: name.into(),
        vector: vec![0.; 50],
        filter: Some(filter),
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    };

    let search_points_response = client.search_points(&search_points_request).await?;
    // dbg!(&search_points_response);

    let mut file = File::create("output_search.txt")?;
    writeln!(file, "{:#?}", &search_points_response)?;
    drop(file);
    println!("Search result has been written to output_search.txt");

    visualize(&search_points_response).await?;

    Ok(search_points_response)
}

async fn visualize(search_points_response: &SearchResponse) -> Result<()> {
    for (_i, point) in search_points_response.result.iter().enumerate() {
        println!("{:?}, {:?}, \"Age: {:?}\", {:?}",
                 point.payload["ID"].as_str().unwrap(),
                 point.payload["Name"].as_str().unwrap(),
                 point.payload["Age"].as_integer().unwrap(),
                 point.payload["Others"].as_struct().unwrap()
        );
    }
    Ok(())
}

async fn aggregation(search_points_response: SearchResponse) -> Result<()> {
    let ages: Vec<f64> =
        search_points_response.result.iter().map(|point| {
            let payload = &point.payload;
            let age = payload["Age"].as_integer().unwrap() as f64;
            age
        }
        ).collect();

    let average_age = ages.iter().sum::<f64>() / ages.len() as f64;

    println!("Average age: {}", average_age);

    Ok(())
}


